package com.demo.cicd.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MvcController {

    @GetMapping("/")
    public String homepage(
        @RequestParam(name="name", defaultValue = "John Doe") String userName,
        Model model) {

        model.addAttribute("name", userName);
        return "index";
    }
}
